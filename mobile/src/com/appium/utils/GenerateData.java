package com.appium.utils;

import org.apache.commons.lang.RandomStringUtils;
import java.util.Date;

public class GenerateData {

    public final String random = "" + (new Date().getTime());

    public final String email() {
        String email = "automation@travelstart.com";
        return email;

    }
    
    public final String email2() {
        String email2 = "autotravelstart+" + random + "@gmail.com";
        return email2;
    }

    public static final String firstName(){
        String firstName ="Roger";
        return firstName;
    }

    public static final String accountholderName(){
        String accountholderName ="Roger R Morgado";
        return accountholderName;
    }

    public final String surname() {
        String surname = RandomStringUtils.randomAlphabetic(7);
        return surname;
    }

    public final String telnumber() {
        String telnumber = "0828479967";
        return telnumber;
    }

    public final String affiliatename() {
        String affiliatename = "automation" + RandomStringUtils.randomNumeric(5);
        return affiliatename;
    }

    public final String affiliatecode() {
        String affiliatecode = "automationbeta" + RandomStringUtils.randomNumeric(5);
        return affiliatecode;
    }

    public final String randomNumber() {
        String randomNumber = RandomStringUtils.randomNumeric(7);
        return randomNumber;
    }

    public static final String password() {
        String password = "pa55word";
        return password;
    }

    public static final String creditcardNumber() {
        String creditcard = "4242424242424242";
        return creditcard;
    }

    public static final String streetAddress() {
        String streetaddress = "7th Floor, 56 Shortmarket Street";
        return streetaddress;
    }

    public static final String postalCode() {
        String postalcode = "8000";
        return postalcode;
    }

    public static final String city() {
        String city = "Cape Town";
        return city;
    }
 
}