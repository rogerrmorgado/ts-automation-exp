package com.appium.utils;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import io.appium.java_client.SwipeElementDirection;
import org.testng.annotations.Test;
import java.util.List;
import org.testng.Assert;
import org.openqa.selenium.JavascriptExecutor;
import java.util.HashMap;
import com.appium.utils.WaitFor;
import org.openqa.selenium.By;
import com.appium.helper.DriverFactory;
import java.util.Map;
import com.google.common.collect.ImmutableMap;
import io.appium.java_client.TouchAction;
import java.util.HashMap;
import org.openqa.selenium.interactions.HasTouchScreen;
import org.openqa.selenium.interactions.TouchScreen;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.remote.RemoteTouchScreen;
import org.openqa.selenium.Dimension;


public class Swipe extends DriverFactory{
    
  WaitFor wait = new WaitFor();
    DriverFactory driverfactory = new DriverFactory();
    AppiumDriver driver = driverfactory.getDriver();

    public Swipe() throws InterruptedException{

    }

   private Point getCenter(MobileElement element) {

      Point upperLeft = element.getLocation();
      Dimension dimensions = element.getSize();
      return new Point(upperLeft.getX() + dimensions.getWidth()/2, upperLeft.getY() + dimensions.getHeight()/2);

    }

    public void swipeUp(MobileElement element) {

    if(propertyFileName == "iOS"){  

       Point location1 = element.getLocation();
       Point center = getCenter(element);
       driver.swipe(center.getX(), center.getY(), center.getX(), center.getY()-200, 1000);

    }

    if(propertyFileName == "android") {

      element.swipe(SwipeElementDirection.UP,2000);

  }

}

    public void swipeDown(MobileElement element) {

    if(propertyFileName == "iOS"){  

       Point location1 = element.getLocation();
       Point center = getCenter(element);
       driver.swipe(center.getX(), center.getY(), center.getX(), center.getY()+200, 1000);

}

    if(propertyFileName == "android") {

      element.swipe(SwipeElementDirection.DOWN,2000);

  }

}

    public void swipeLeft(MobileElement element) {

    if(propertyFileName == "iOS"){      

       element.swipe(SwipeElementDirection.LEFT, 150, 20, 1000);

}

    if(propertyFileName == "android") {

       element.swipe(SwipeElementDirection.LEFT,2000);

  }

}

    public void swipeRight(MobileElement element) {

    if(propertyFileName == "iOS"){ 

      element.swipe(SwipeElementDirection.RIGHT,2000);

}

    if(propertyFileName == "android"){  
    
      element.swipe(SwipeElementDirection.RIGHT,2000);

   }

  }

}
