package com.appium.utils;

import java.util.concurrent.TimeUnit;
import com.appium.helper.DriverFactory;

import org.json.JSONException; 
import org.openqa.selenium.By; 
import org.openqa.selenium.NoSuchElementException; 
import org.openqa.selenium.NotFoundException; 
import org.openqa.selenium.TimeoutException; 
import org.openqa.selenium.WebDriver; 
import org.openqa.selenium.WebDriverException; 
import org.openqa.selenium.WebElement; 
import org.openqa.selenium.support.ui.FluentWait; 
import org.openqa.selenium.support.ui.Wait; 
import org.openqa.selenium.support.ui.WebDriverWait;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;

import com.google.common.base.Function; 

  public class WaitFor extends DriverFactory {
      private static final int MAX_WAIT_SECONDS = 90;
      private static final int MAX_POLLING_SECONDS = 1;
      private static int waitTime = 60;
      DriverFactory driverfactory = new DriverFactory();
      AppiumDriver driver = driverfactory.getDriver();

public MobileElement waitForiOSElementToAppear(final By locator) {

  Wait wait = new FluentWait(driver)
  .withTimeout(MAX_WAIT_SECONDS, TimeUnit.SECONDS)
  .pollingEvery(MAX_POLLING_SECONDS, TimeUnit.SECONDS)
  .ignoring(NoSuchElementException.class);

  MobileElement element = null;

  try {
    element = (MobileElement) wait.until(new Function<IOSDriver, MobileElement>() {

      @Override
      public MobileElement apply(IOSDriver driver)
      {
        return (MobileElement) driver.findElement(locator);
      }

    });
  }

  catch (TimeoutException e) {

    try {
    
      driver.findElement(locator);
    }
    catch (NoSuchElementException renamedErrorOutput) {
      
      renamedErrorOutput.addSuppressed(e);

      throw renamedErrorOutput;
    }
    e.addSuppressed(e);
    throw new NoSuchElementException("Timeout reached when searching for element", e);
  }

  return element;

     }

public MobileElement waitForAndroidElementToAppear(final By locator) {

  Wait wait = new FluentWait(driver)
  .withTimeout(MAX_WAIT_SECONDS, TimeUnit.SECONDS)
  .pollingEvery(MAX_POLLING_SECONDS, TimeUnit.SECONDS)
  .ignoring(NoSuchElementException.class);

  MobileElement element = null;

  try {
    element = (MobileElement) wait.until(new Function<AndroidDriver, MobileElement>() {

      @Override
      public MobileElement apply(AndroidDriver driver)
      {
        return (MobileElement) driver.findElement(locator);
      }

    });
  }

  catch (TimeoutException e) {

    try {
    
      driver.findElement(locator);
    }
    catch (NoSuchElementException renamedErrorOutput) {
      
      renamedErrorOutput.addSuppressed(e);

      throw renamedErrorOutput;
    }
    e.addSuppressed(e);
    throw new NoSuchElementException("Timeout reached when searching for element", e);
  }

  return element;

     }

  public MobileElement waitForMobileElementToAppear(final By locator) {

  Wait<AppiumDriver> wait = new FluentWait<AppiumDriver>(driver)
  .withTimeout(MAX_WAIT_SECONDS, TimeUnit.SECONDS)
  .pollingEvery(MAX_POLLING_SECONDS, TimeUnit.SECONDS)
  .ignoring(NoSuchElementException.class);

  MobileElement element = null;

  try {
    element = (MobileElement) wait.until(new Function<AppiumDriver, MobileElement>() {

      @Override
      public MobileElement apply(AppiumDriver driver)
      {
        return (MobileElement) driver.findElement(locator);
      }

    });
  }

  catch (TimeoutException e) {

    try {
    
      driver.findElement(locator);
    }
    catch (NoSuchElementException renamedErrorOutput) {
      
      renamedErrorOutput.addSuppressed(e);

      throw renamedErrorOutput;
    }
    e.addSuppressed(e);
    throw new NoSuchElementException("Timeout reached when searching for element", e);
  }

  return element;

     }

 public MobileElement waitUntilMobileElementDisplays(final By locator) {

         MobileElement element = null;
         WebDriverWait wait = new WebDriverWait(driver, waitTime);

         try {

         element = (MobileElement) wait.until(ExpectedConditions.visibilityOfElementLocated(locator));

       }

  catch (NoSuchElementException e) {
      System.out.println(e);

     }

  return (MobileElement) element;

 }

  public Alert waitUntilAlertDisplays() {

         Alert element = null;
         WebDriverWait wait = new WebDriverWait(driver, waitTime);

         try {

         element = wait.until(ExpectedConditions.alertIsPresent());

       }

  catch (TimeoutException e) {
      System.out.println(e);

     }

  return element;

 }

    public boolean isAlertPresent() { 

        try { 
            driver.switchTo().alert(); 
            return true; 
        }  
        catch (NoAlertPresentException Ex) {
            return false; 
        }
    } 

   public MobileElement waitUntilMobileElementisClickable(final By locator) {

         MobileElement element = null;
         WebDriverWait wait = new WebDriverWait(driver, waitTime);

         try {

         element = (MobileElement) wait.until(ExpectedConditions.elementToBeClickable(locator));

       }

  catch (NoSuchElementException e) {
      System.out.println(e);

     }

  return element;

 }

   public boolean waitUntilMobileElementisInvisible(final By locator) {

         Boolean element = null;
         WebDriverWait wait = new WebDriverWait(driver, waitTime);

         try {

         element = wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));

       }

  catch (NoSuchElementException e) {
      System.out.println(e);

     }

  return element;

 }

public MobileElement waitForiOSAccessIDToAppear(final String locator) {

  Wait wait = new FluentWait(driver)
  .withTimeout(MAX_WAIT_SECONDS, TimeUnit.SECONDS)
  .pollingEvery(MAX_POLLING_SECONDS, TimeUnit.SECONDS)
  .ignoring(NoSuchElementException.class);

  MobileElement element = null;

  try {
    element = (MobileElement) wait.until(new Function<IOSDriver, MobileElement>() {

      @Override
      public MobileElement apply(IOSDriver driver)
      {
        return (MobileElement) driver.findElementByAccessibilityId(locator);
      }

    });
  }

  catch (TimeoutException e) {

    try {
    
      driver.findElementByAccessibilityId(locator);
    }
    catch (NoSuchElementException renamedErrorOutput) {
      
      renamedErrorOutput.addSuppressed(e);

      throw renamedErrorOutput;
    }
    e.addSuppressed(e);
    throw new NoSuchElementException("Timeout reached when searching for element", e);
  }

  return element;

     }

  public MobileElement waitForAndroidAccessIDToAppear(final String locator) {

  Wait wait = new FluentWait(driver)
  .withTimeout(MAX_WAIT_SECONDS, TimeUnit.SECONDS)
  .pollingEvery(MAX_POLLING_SECONDS, TimeUnit.SECONDS)
  .ignoring(NoSuchElementException.class);

  MobileElement element = null;

  try {
    element = (MobileElement) wait.until(new Function<AndroidDriver, MobileElement>() {

      @Override
      public MobileElement apply(AndroidDriver driver)
      {
        return (MobileElement) driver.findElementByAccessibilityId(locator);
      }

    });
  }

  catch (TimeoutException e) {

     try {
    
      driver.findElementByAccessibilityId(locator);
    }
    catch (NoSuchElementException renamedErrorOutput) {
      
      renamedErrorOutput.addSuppressed(e);

      throw renamedErrorOutput;
    }
    e.addSuppressed(e);
    throw new NoSuchElementException("Timeout reached when searching for element", e);
  }

  return element;

     }

  public boolean isElementPresent(final By locator) {

    try {

       Wait wait = new FluentWait(driver)
       .withTimeout(3, TimeUnit.SECONDS)
       .pollingEvery(1, TimeUnit.SECONDS);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        return true;
    }
    catch (NoSuchElementException e){
          System.out.println(e);
        return false;
    }

 }

   public boolean isWaitForElementPresent(final By locator) {

      Wait wait = new FluentWait(driver)
      .withTimeout(3, TimeUnit.SECONDS)
      .pollingEvery(1, TimeUnit.SECONDS);

    try {

       Boolean element = null;

       element = (Boolean) wait.until(new Function<AndroidDriver, Boolean>() {

      @Override
      public Boolean apply(AndroidDriver driver)
      {

        driver.findElement(locator);
        return true;

          }

        });
    }
   catch (NoSuchElementException e) {
        System.out.println(e);

  }

  return false;

     }

}

