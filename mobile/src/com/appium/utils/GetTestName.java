package com.appium.utils;

import java.util.concurrent.TimeUnit;
import com.appium.helper.DriverFactory;
import org.json.JSONException; 
import org.openqa.selenium.By; 
import org.openqa.selenium.NoSuchElementException; 
import org.openqa.selenium.NotFoundException; 
import org.openqa.selenium.TimeoutException; 
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.support.ui.FluentWait; 
import com.appium.utils.Log;
import com.google.common.base.Function; 


public class GetTestName extends DriverFactory {

  public static String getTestCaseName(String sTestCase) throws Exception {

    String value = sTestCase;

    try{

      int posi = value.indexOf("@");
      value = value.substring(0, posi);
      posi = value.lastIndexOf(".");  
      value = value.substring(posi + 1);
      return value;
        }catch (Exception e){
      Log.error("Class Utils | Method getTestCaseName | Exception desc : "+e.getMessage());
      throw (e);
          }
      }

}

