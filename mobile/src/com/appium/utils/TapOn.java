package com.appium.utils;

import com.appium.helper.DriverFactory;

import org.openqa.selenium.By; 
import org.openqa.selenium.NoSuchElementException; 
import org.openqa.selenium.NotFoundException; 
import org.openqa.selenium.TimeoutException; 
import org.openqa.selenium.WebDriverException; 
import io.appium.java_client.MobileElement;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.TouchAction;

import com.google.common.base.Function; 


public class TapOn extends DriverFactory {
  DriverFactory driverfactory = new DriverFactory();
  AppiumDriver driver = driverfactory.getDriver();


public void tapOn(MobileElement element) {

   try {

        new TouchAction(driver).tap(element).perform();
  
    } catch (Exception e) {
         System.out.println(e);

     }

   }

   public void pressOn(MobileElement element) {

   try {

        new TouchAction(driver).press(element).perform();
  
    } catch (Exception e) {
         System.out.println(e);

     }

   }

      public void moveTo(MobileElement element) {

   try {

        new TouchAction(driver).moveTo(element).perform();
  
    } catch (Exception e) {
         System.out.println(e);

     }

   }
 
}
