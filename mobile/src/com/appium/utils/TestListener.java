package com.appium.utils;

import org.testng.*;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;


import java.util.Iterator;


public class TestListener implements ITestListener{
    @Override
    public void onFinish(ITestContext context) {

        Iterator<ITestResult> listOfFailedTests = context.getFailedTests().getAllResults().iterator();

        while (listOfFailedTests.hasNext()) {
            ITestResult failedTest = listOfFailedTests.next();
            ITestNGMethod method = failedTest.getMethod();
            System.out.println("Retrying the following failed test: " + method);

            if (context.getFailedTests().getResults(method).size() > 1) {
                listOfFailedTests.remove();

            }
            else{
                if (context.getPassedTests().getResults(method).size() > 0) {
                    listOfFailedTests.remove();

                }
            }
        }
    }

    public void onTestStart(ITestResult result) {

    }

    public void onTestSuccess(ITestResult result) {

    }

    public void onTestFailure(ITestResult result) {


    }

    public void onTestSkipped(ITestResult result) {

    }

    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

    }

    public void onStart(ITestContext context) {


    }

}
