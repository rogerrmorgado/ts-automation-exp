package com.appium.utils;
import com.appium.helper.DriverFactory;


public class MOBILE_CONSTANTS extends DriverFactory {

    public static final String Path_TestData = PROJECT_PATH + "/mobile/src/com/appium/data/";
    public static final String File_TestData = "TestCasesMobi.xlsx";  
    public static final String DB_USER = "root";
    public static final String DB_PASSWORD = "root";
    public static String DB_IPPORT = "localhost:7706"; 
    public static String DB_NAME = "user";
    public static String TCC_HOST = "beta.travelstart.com";
    public static String TCC_USERNAME = "roger";
    public static String TCC_PASSWORD = "RogerM@2016";
    public static final String MQ_HOST = "betamq.travelstart.com";
    public static final int MQ_PORT = 8161;
    public static final String Path_ScreenShot = PROJECT_PATH + "/mobile/src/com/appium/resources/";
    public static final String HOST_BETA_ZA = "betaza.travelstart.com";
    public static final String HOST_BETA_SA = "betasa.travelstart.com";
    public static final String HOST_BETA_NG = "betang.travelstart.com";
    public static final String HOST_BETA_EG = "betaeg.travelstart.com";
    public static final String HOST_BETA_TK = "beta.geziko.com";
    public static final String HOST_LIVE_ZA = "www.travelstart.co.za";
    public static final String HOST_LIVE_SA = "www.travelstart.com.sa";
    public static final String HOST_LIVE_NG = "www.travelstart.com.ng";
    public static final String HOST_LIVE_EG = "www.travelstart.com.eg";
    public static final String HOST_LIVE_TK = "www.geziko.com";

}