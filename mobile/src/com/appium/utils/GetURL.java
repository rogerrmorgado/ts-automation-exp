package com.appium.utils;

import org.apache.commons.lang.RandomStringUtils;
import java.io.*;
import com.appium.utils.MOBILE_CONSTANTS;
import com.appium.helper.DriverFactory;
import java.nio.charset.Charset;
import java.nio.file.Files;
import io.appium.java_client.AppiumDriver;

public class GetURL extends DriverFactory {
    DriverFactory driverfactory = new DriverFactory();
    AppiumDriver driver = driverfactory.getDriver();

public void getURL(String sRegion) throws IOException {

if(BETA == true) {

    if (sRegion.equalsIgnoreCase("ZA")) {
        driver.get("https://" + MOBILE_CONSTANTS.HOST_BETA_ZA);

        } else if (sRegion.equalsIgnoreCase("SA")) {
            driver.get("https://" + MOBILE_CONSTANTS.HOST_BETA_SA);

        } else if (sRegion.equalsIgnoreCase("NG")) {
            driver.get("https://" + MOBILE_CONSTANTS.HOST_BETA_NG);

        } else if (sRegion.equalsIgnoreCase("EG")) {
            driver.get("https://" + MOBILE_CONSTANTS.HOST_BETA_EG);

        } else if (sRegion.equalsIgnoreCase("TK")) {
            driver.get("https://" + MOBILE_CONSTANTS.HOST_BETA_TK);

        } else {
            System.out.println("Cannot find region");

        }

} else if (BETA == false) {

    if (sRegion.equalsIgnoreCase("ZA")) {
        driver.get("https://" + MOBILE_CONSTANTS.HOST_LIVE_ZA);

        } else if (sRegion.equalsIgnoreCase("SA")) {
            driver.get("https://" + MOBILE_CONSTANTS.HOST_LIVE_SA);

        } else if (sRegion.equalsIgnoreCase("NG")) {
            driver.get("https://" + MOBILE_CONSTANTS.HOST_LIVE_NG);

        } else if (sRegion.equalsIgnoreCase("EG")) {
            driver.get("https://" + MOBILE_CONSTANTS.HOST_LIVE_EG);

        } else if (sRegion.equalsIgnoreCase("TK")) {
            driver.get("https://" + MOBILE_CONSTANTS.HOST_LIVE_TK);

        } else {
            System.out.println("Cannot find region");

        }
    }

  }

}