package com.appium.utils;

import java.util.concurrent.TimeUnit;
import com.appium.helper.DriverFactory;

import org.json.JSONException; 
import org.openqa.selenium.By; 
import org.openqa.selenium.NoSuchElementException; 
import org.openqa.selenium.NotFoundException; 
import org.openqa.selenium.TimeoutException; 
import org.openqa.selenium.WebDriverException; 
import org.openqa.selenium.WebElement;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.OutputType;
import com.appium.utils.MOBILE_CONSTANTS;
import org.apache.commons.io.FileUtils;
import java.io.File;
import com.appium.utils.Log;

import com.google.common.base.Function; 

public class TakeScreenshot extends DriverFactory {
	 DriverFactory driverfactory = new DriverFactory();
   AppiumDriver driver = driverfactory.getDriver();
     
       public void takeScreenshot(String sTestCase) throws Exception {

      try{
        
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File(MOBILE_CONSTANTS.Path_ScreenShot + sTestCase +".jpg"));  
      } catch (Exception e){
        Log.error("Class Utils | Method takeScreenshot | Exception occured while capturing ScreenShot : "+e.getMessage());
        throw new Exception();
      }
    }

}

