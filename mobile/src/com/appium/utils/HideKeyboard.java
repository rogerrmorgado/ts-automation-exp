package com.appium.utils;

import com.appium.helper.DriverFactory;

import org.openqa.selenium.By; 
import org.openqa.selenium.NoSuchElementException; 
import org.openqa.selenium.NotFoundException; 
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.AppiumDriver;

import com.google.common.base.Function; 


public class HideKeyboard extends DriverFactory {
	DriverFactory driverfactory = new DriverFactory();
    AppiumDriver driver = driverfactory.getDriver();

public void hideAndroidKeyboard(boolean test) {

 if (test == true) {

    System.out.println("No keyboard to hide");

    } else {
     driver.hideKeyboard();
   
    }

  }
 
}
