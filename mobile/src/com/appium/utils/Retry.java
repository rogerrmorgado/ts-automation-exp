package com.appium.utils;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;
import org.testng.*;


public class Retry implements IRetryAnalyzer {
    private int retryCount= 0;
    private int maxRetryCount= 1;

    @Override
    public boolean retry(ITestResult result) {
        if (retryCount <maxRetryCount) {
            retryCount++;
            return true;
        }
        return false;

    }

}