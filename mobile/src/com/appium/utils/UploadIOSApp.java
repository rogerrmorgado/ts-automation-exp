package com.appium.utils;

import com.appium.helper.DriverFactory;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.FileEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.openqa.selenium.WebDriver;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.UnexpectedException;
import org.json.JSONException;
import org.json.JSONObject;


public class UploadIOSApp extends DriverFactory {

    public String execute(WebDriver driver) throws IOException {

        String filename = "travelstart.zip";
        File file = new File("/Users/roger/travelstart.zip");
        Boolean overwrite = true;

        HttpClient httpclient = new DefaultHttpClient();

        HttpPost post = new HttpPost("http://saucelabs.com/rest/v1/storage/" +
                username + "/" + filename + "?overwrite=" + overwrite.toString());

        FileEntity entity = new FileEntity(file);
        entity.setContentType(new BasicHeader("Content-Type", "application/octet-stream"));
        post.setEntity(entity);

        System.out.println("Busy uploading iOS app to sauce temporary storage...");

        post.setHeader("Content-Type", "application/octet-stream");
        post.setHeader("Authorization", encodeAuthentication());
        HttpResponse response = httpclient.execute(post);

        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        String line;
        StringBuilder builder = new StringBuilder();
        while ((line = rd.readLine()) != null) {
            builder.append(line);

        }

        try {

            JSONObject sauceUploadResponse = new JSONObject(builder.toString());
            if (sauceUploadResponse.has("error")) {
                throw new UnexpectedException("Failed to upload to sauce-storage: "
                        + sauceUploadResponse.getString("error"));
            }
            return sauceUploadResponse.getString("md5");
        } catch (JSONException j) {
            throw new UnexpectedException("Failed to parse json response.", j);
        }

    }

    private String encodeAuthentication() {
        String auth = username + ":" + accessKey;
        auth = "Basic " + new String(Base64.encodeBase64(auth.getBytes()));
        System.out.println(auth);
        return auth;
    }


}



