package com.appium.tests;

import com.appium.helper.DriverFactory;
import com.appium.pages.TSAPP.HomeScreen;
import com.appium.pages.TSAPP.NavbarScreen;
import org.testng.annotations.Test;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import com.appium.helper.manager.ParallelThread;


public class additemtoCartTest extends DriverFactory {


    @Test(groups = {"android", "iOS"})
        public void testadditemtoCartTest(Method name) throws InterruptedException, Exception {
        System.out.println("Test case started");

       setupInstance("", "Android", "4.4", "Samsung Galaxy S4 Emulator", "android", name.getName());

        //Go to home screen
        HomeScreen home = new HomeScreen();
        home.acceptStartupDialog();
        home.gotoNavbar();

        //Go to register screen
        NavbarScreen navbar = new NavbarScreen();
        navbar.registerButton();

        System.out.println("Test is successful");


    }
}


