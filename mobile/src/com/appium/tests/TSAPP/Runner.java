package com.appium.tests;

import org.testng.annotations.Test;
import com.appium.helper.manager.ParallelThread;
import java.util.ArrayList;
import java.util.List;
import com.appium.helper.DriverFactory;

public class Runner {
    
      @Test(groups = {"android", "iOS"})
      public static void main(String[] args) throws Exception {
        ParallelThread parallelThread = new ParallelThread();
	    parallelThread.runner("com.appium.tests");

 }

}