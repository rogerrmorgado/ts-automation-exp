package com.appium.pages.TSAPP;

import io.appium.java_client.AppiumDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import com.appium.utils.WaitFor;
import com.appium.helper.DriverFactory;
import com.appium.utils.PropertyFileReader;
import org.openqa.selenium.support.ui.WebDriverWait;
import io.appium.java_client.TouchAction;
import org.openqa.selenium.Alert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import io.appium.java_client.MobileElement;
import io.appium.java_client.MultiTouchAction;
import org.openqa.selenium.JavascriptExecutor;
import java.util.HashMap;

import javax.xml.ws.WebEndpoint;

import static org.junit.Assert.*;

public class HomeScreen extends DriverFactory {
	
	PropertyFileReader prop = new PropertyFileReader();
    WaitFor waitSuper = new WaitFor();
    DriverFactory driverfactory = new DriverFactory();
    AppiumDriver driver = driverfactory.getDriver();

	By noDailyDeals = null;
	By navigationBarButton = null;
	By signInButton = null;
	By userName = null;
	By password = null;
	By loginButton = null;
	By done = null;
	By next = null;
	By loggedInState = null;
	By homeButton = null;
    By allDailyDeals = null;
    By searchButton = null;
    By cartButton = null;
    By wishlistButton = null;
    By accountButton = null;
    By departments = null;
    By searchField = null;
    By dailyDeals = null;
    By nothanksButton = null;
    By takealotlogo = null;

	public HomeScreen () throws InterruptedException{
		Thread.sleep(8000);

		PropertyFileReader prop = new PropertyFileReader();
        WaitFor waitSuper = new WaitFor();
        
		if(propertyFileName == "android"){
		    allDailyDeals = By.xpath(prop.returnPropVal("allDailyDeals"));
            dailyDeals = By.xpath(prop.returnPropVal("dailyDeals"));
            departments = By.xpath(prop.returnPropVal("dailyDeals"));
            navigationBarButton = By.xpath(prop.returnPropVal("navigationBarButton"));
            searchField = By.xpath(prop.returnPropVal("searchField"));
            homeButton = By.xpath(prop.returnPropVal("homeButton"));
            cartButton = By.xpath(prop.returnPropVal("cartButton"));
            wishlistButton = By.name(prop.returnPropVal("wishlistButton"));
            searchButton = By.name(prop.returnPropVal("searchButton"));
            accountButton = By.name(prop.returnPropVal("accountButton"));
            nothanksButton = By.name(prop.returnPropVal("nothanksButton"));
            takealotlogo = By.name(prop.returnPropVal("takealotlogo"));

		}
        
		if(propertyFileName == "iOS"){
			allDailyDeals = By.xpath(prop.returnPropVal("allDailyDeals"));
			dailyDeals = By.xpath(prop.returnPropVal("dailyDeals"));
			departments = By.xpath(prop.returnPropVal("dailyDeals"));
			navigationBarButton = By.xpath(prop.returnPropVal("navigationBarButton"));
			searchField = By.xpath(prop.returnPropVal("searchField"));
			homeButton = By.xpath(prop.returnPropVal("homeButton"));
			cartButton = By.xpath(prop.returnPropVal("cartButton"));
			wishlistButton = By.name(prop.returnPropVal("wishlistButton"));
			searchButton = By.name(prop.returnPropVal("searchButton"));
			accountButton = By.name(prop.returnPropVal("accountButton"));
            nothanksButton = By.xpath(prop.returnPropVal("nothanksButton"));
            takealotlogo = By.name(prop.returnPropVal("takealotlogo"));
		}

		
	}

    public void acceptStartupDialog() throws InterruptedException {

        if(propertyFileName == "iOS"){

       Alert alert = waitSuper.waitUntilAlertDisplays();
       alert = driver.switchTo().alert();
       alert.accept();

       MobileElement confirm = waitSuper.waitForiOSAccessIDToAppear("Next time");
       Thread.sleep(1000);
       confirm.click();

    }

        if(propertyFileName == "android"){

        MobileElement alert = waitSuper.waitForAndroidElementToAppear(By.xpath("//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.Button[2]"));
        Thread.sleep(1000);
        alert.click();


    }

}

    public void gotoNavbar() throws InterruptedException {

    	if(propertyFileName == "iOS"){
        Thread.sleep(50000);
        //MobileElement nav = waitSuper.waitForiOSAccessIDToAppear(By.xpath("//UIAApplication[1]/UIAWindow[2]/UIANavigationBar[1]/UIAButton[1]"));
        //nav.click();

    }

        if(propertyFileName == "android"){
        MobileElement nav = waitSuper.waitForAndroidAccessIDToAppear("Navigate up");
        Thread.sleep(1000);
        nav.click();

    }

}

}
