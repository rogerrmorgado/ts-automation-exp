package com.appium.pages.TSAPP;

import io.appium.java_client.AppiumDriver;
import com.appium.helper.DriverFactory;
import com.appium.utils.PropertyFileReader;
import com.appium.utils.GenerateData;
import com.appium.utils.WaitFor;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import io.appium.java_client.MobileElement;

public class NavbarScreen extends DriverFactory{

    GenerateData data = new GenerateData();
    WaitFor waitSuper = new WaitFor();
    DriverFactory driverfactory = new DriverFactory();
    AppiumDriver driver = driverfactory.getDriver();

    PropertyFileReader prop = new PropertyFileReader();


    By homeButton = null;
    By departmentButton = null;
    By dailyDealButton = null;
    By accountButton = null;
    By cartButton = null;
    By wishlistButton = null;
    By ordersButton = null;
    By notificationButton = null;
    By helpFAQButton = null;
    By contactUsButton = null;
    By loginButton = null;
    By registerButton = null;
    By logoutButton = null;

    public NavbarScreen() throws InterruptedException {
        Thread.sleep(4000);

        PropertyFileReader prop = new PropertyFileReader();

        if(propertyFileName == "iOS"){
        homeButton = By.xpath(prop.returnPropVal("homeButton"));
        departmentButton = By.name(prop.returnPropVal("departmentButton"));
        dailyDealButton = By.name(prop.returnPropVal("dailyDealButton"));
        accountButton = By.name(prop.returnPropVal("accountButton"));
        cartButton = By.name(prop.returnPropVal("cartButton"));
        wishlistButton = By.name(prop.returnPropVal("wishlistButton"));
        ordersButton = By.name(prop.returnPropVal("ordersButton"));
        notificationButton = By.name(prop.returnPropVal("notificationButton"));
        helpFAQButton = By.name(prop.returnPropVal("helpFAQButton"));
        contactUsButton = By.name(prop.returnPropVal("contactUsButton"));
        loginButton = By.name(prop.returnPropVal("loginButton"));
        registerButton = By.xpath(prop.returnPropVal("registerButton"));
        logoutButton = By.name(prop.returnPropVal("logoutButton"));
    }

        if(propertyFileName == "android"){
        homeButton = By.name(prop.returnPropVal("homeButton"));
        departmentButton = By.name(prop.returnPropVal("departmentButton"));
        dailyDealButton = By.name(prop.returnPropVal("dailyDealButton"));
        accountButton = By.name(prop.returnPropVal("accountButton"));
        cartButton = By.name(prop.returnPropVal("cartButton"));
        wishlistButton = By.name(prop.returnPropVal("wishlistButton"));
        ordersButton = By.name(prop.returnPropVal("ordersButton"));
        notificationButton = By.name(prop.returnPropVal("notificationButton"));
        helpFAQButton = By.name(prop.returnPropVal("helpFAQButton"));
        contactUsButton = By.name(prop.returnPropVal("contactUsButton"));
        loginButton = By.name(prop.returnPropVal("loginButton"));
        registerButton = By.name(prop.returnPropVal("registerButton"));
        logoutButton = By.name(prop.returnPropVal("logoutButton"));
    }

}

      public void registerButton() throws InterruptedException {

        if(propertyFileName == "iOS"){

        System.out.println("test 2...");
        MobileElement register = waitSuper.waitForiOSElementToAppear(registerButton);
        Thread.sleep(5000);
        driver.findElement(registerButton).click();

    }

        if(propertyFileName == "android"){
        MobileElement regis = waitSuper.waitForAndroidElementToAppear(By.xpath("//*[starts-with(@text, 'Register')]"));
        //wait.until(ExpectedConditions.presenceOfElementLocated(registerButton));
        //Thread.sleep(1000);
        regis.click();

    }
    
  }

}
