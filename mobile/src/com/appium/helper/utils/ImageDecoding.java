package com.appium.helper.utils;

//import sun.misc.BASE64Encoder;
import java.util.Base64;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
/**
 * Created by saikrisv on 10/06/16.
 */
public class ImageDecoding {
    public static String encodeToString(BufferedImage image, String type) {
        String imageString = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            ImageIO.write(image, type, bos);
            byte[] imageBytes = bos.toByteArray();

            //BASE64Encoder encoder = new BASE64Encoder();
            //imageString = encoder.encode(imageBytes);
            imageString = new String(Base64.getEncoder().encode(imageBytes));

            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageString;
    }
}
