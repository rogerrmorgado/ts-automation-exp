package com.appium.helper;

import com.appium.utils.UploadIOSApp;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.FileEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.WebDriver;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.rmi.UnexpectedException;
import com.appium.helper.DriverFactory;
import com.appium.utils.UploadAndroidApp;
import org.openqa.selenium.remote.RemoteWebDriver;


public class UploadFactory {

    private static WebDriver driver;
    public static String appType = "";

    public static void main(String[] args) throws IOException {
        appType = System.getProperty("appType");
        System.out.println("App Type: " + appType);
        System.out.println("Executing functionality for mobile app upload....");

        if (appType.equalsIgnoreCase("iOS")) {
            try {
                new UploadIOSApp().execute(driver);
                System.out.println("SUCCESS: " +  appType + " app uploaded");
            } catch (Exception ex) {
                System.out.println(ex);
                System.out.println("ERROR: Failed to upload to " + appType + " app to SauceLabs!");
            }
        }

        if (appType.equalsIgnoreCase("android")) {
            try {
                new UploadAndroidApp().execute(driver);
                System.out.println("SUCCESS: " + appType + " app uploaded");
            } catch (Exception ex) {
                System.out.println(ex);
                System.out.println("ERROR: Failed to upload to " + appType + " app to SauceLabs!");
            }
        }

    }
}


