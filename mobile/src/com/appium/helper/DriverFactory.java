package com.appium.helper;

import com.saucelabs.saucerest.SauceREST;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.TestNG;
import org.testng.annotations.*;
//import com.appium.utils.ExcelReader;
import org.openqa.selenium.*;
import com.appium.utils.GetTestName;
import org.apache.log4j.xml.DOMConfigurator;
import com.appium.utils.MOBILE_CONSTANTS;
import io.appium.java_client.MobileElement;
import com.appium.utils.Log;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import java.util.concurrent.TimeUnit;
import com.appium.helper.manager.AppiumManager;
import com.appium.helper.manager.AndroidDeviceConfiguration;
import com.appium.helper.manager.AppiumParallelTest;
import java.lang.reflect.Method;
import java.net.URISyntaxException;

public class DriverFactory {

    public static AppiumDriver<MobileElement> driver;
    private static ThreadLocal<AppiumDriver> appiumDriver = new ThreadLocal<AppiumDriver>();
    public String sTestCase;
    public static String MOBI_SERVER = System.getProperty("MOBI_SERVER");
    public static boolean SAUCELABS_SIMULATOR = Boolean.valueOf(System.getProperty("SAUCELABS_SIMULATOR"));
    public static boolean LOCAL_SIMULATOR = Boolean.valueOf(System.getProperty("LOCAL_SIMULATOR"));
    public static boolean LOCAL_DEVICE = Boolean.valueOf(System.getProperty("LOCAL_DEVICE"));
    public static boolean SAUCELABS_DEVICE = Boolean.valueOf(System.getProperty("SAUCELABS_DEVICE"));
    public static String PROJECT_PATH = System.getProperty("user.dir");
    public static boolean BETA = Boolean.valueOf(System.getProperty("BETA"));
    public static String SHEET_NAME = System.getProperty("SHEET_NAME");
    public static String SAUCE_HOST = System.getProperty("SAUCE_HOST");
    public static String SAUCE_PORT = System.getProperty("SAUCE_PORT");
    public static String TAG = System.getProperty("TAG");
    public static String EMULATOR = System.getProperty("EMULATOR");
    public static String DEVICE = System.getProperty("DEVICE");
    public static boolean recordScreenshots = Boolean.valueOf(System.getProperty("recordScreenshots"));
    public static boolean recordVideo = Boolean.valueOf(System.getProperty("recordVideo"));
    public static String propertyFileName = null;
    public static String username = "travelstart";
    public static String accessKey = "74a253db-fac7-48d7-9f23-7785d935868d";
    public AndroidDeviceConfiguration androidDevice = new AndroidDeviceConfiguration();
    public AppiumManager appiummanager = new AppiumManager();
    public AppiumParallelTest appiumparalleltest = new AppiumParallelTest();

    /** Getting the webdriver */
    public AppiumDriver getDriver() {
        return appiumDriver.get();
    }
 
    /** Setting the webdriver */
    public static void setWebDriver(AppiumDriver driver) {
        appiumDriver.set(driver);
    }


    @BeforeClass()
    public void beforeClass() throws Exception {
        appiumparalleltest.startAppiumServer(getClass().getSimpleName());

    }

    //@BeforeMethod(alwaysRun = true)
    public void setupInstance(String browser, String platform, String version, String device, String app, String name) throws Exception {

        if (LOCAL_SIMULATOR && app.equalsIgnoreCase("android")) {
            androidDevice.startAVD(EMULATOR);
            appiummanager.appiumAndroidServer(name);
            createAppiumDriver(browser, platform, version, device, app);

        } else if (LOCAL_SIMULATOR && app.equalsIgnoreCase("ios")) {
            appiummanager.appiumIOSServer(name);
            createAppiumDriver(browser, platform, version, device, app);

        } else if (LOCAL_DEVICE) {
            //appiumparalleltest.startAppiumServer(getClass().getSimpleName());
            Thread.sleep(3000);
            driver = appiumparalleltest.startAppiumServerInParallel(name);
            appiumparalleltest.startLogResults(name);

        } else if (SAUCELABS_SIMULATOR) {
            createAppiumDriver(browser, platform, version, device, app);

        } else if (SAUCELABS_DEVICE) {
            createAppiumDriver(browser, platform, version, device, app);

        } else {
            System.out.println("No setup necessary"); 

    }

}

    public void createAppiumDriver(String browser, String platform, String version, String device, String app) throws Exception {
        /** This method selects the correct driver */

        System.out.println("Apptype = " + app);

        if (app.equalsIgnoreCase("iOS")) {
            returnIOSDriver(browser, platform, version, device);
            propertyFileName = "iOS";
        }

        if (app.equalsIgnoreCase("android")) {
            returnAndroidDriver(browser, platform, version, device);
            propertyFileName = "android";
        }

        if (app.equalsIgnoreCase("mobiAndroid")) {
            returnAndroidMobiSiteDriver(browser, platform, version, device);
            propertyFileName = "mobi";
        }

        if (app.equalsIgnoreCase("mobiiOS")) {
            returniOSMobiSiteDriver(browser, platform, version, device);
            propertyFileName = "mobi";
        }

        System.out.println("Creating Appium Driver"); 

    }

    public void returnIOSDriver(String browser, String platform, String version, String device) throws MalformedURLException, Exception {
        /** This method returns the iOS driver */

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", platform);
        capabilities.setCapability("name", sTestCase);
        capabilities.setCapability("platformVersion", version);

        if (SAUCELABS_SIMULATOR) {

            System.out.println("iOS on remote simulator");

            capabilities.setCapability(CapabilityType.BROWSER_NAME, "");
            capabilities.setCapability("app", "sauce-storage:travelstart.zip");
            capabilities.setCapability("deviceName", device);
            capabilities.setCapability("recordScreenshots", recordScreenshots);
            capabilities.setCapability("recordVideo", recordVideo);
            capabilities.setCapability("appiumVersion", "1.6.0");

            setWebDriver(new IOSDriver(new URL("http://" + username + ":" + accessKey + "@ondemand.saucelabs.com:80/wd/hub"),
                    capabilities));

        }

        else if (LOCAL_SIMULATOR) {

            System.out.println("iOS on local simulator");

            File app = new File(PROJECT_PATH + "/mobile/resources/travelstart.zip");
            capabilities.setCapability("app", app.getAbsolutePath());
            capabilities.setCapability("browserName", browser);
            capabilities.setCapability("appium-version", "1.5.3");
            capabilities.setCapability("fullReset", true);

            setWebDriver(new IOSDriver(appiummanager.getAppiumUrl(), capabilities));
        }

        else if (SAUCELABS_DEVICE) {

            System.out.println("iOS on remote real device");

            capabilities.setCapability(CapabilityType.BROWSER_NAME, "");
            capabilities.setCapability("platformVersion", version);
            capabilities.setCapability("app", "sauce-storage:flapp.zip");
            capabilities.setCapability("deviceName", device);
            capabilities.setCapability("recordScreenshots", recordScreenshots);
            capabilities.setCapability("recordVideo", recordVideo);
            capabilities.setCapability("appiumVersion", "1.6.0");

            setWebDriver(new IOSDriver(new URL("http://" + username + ":" + accessKey + "@ondemand.saucelabs.com:80/wd/hub"),
                    capabilities));
       
        }

        else {
            System.out.println("No driver available"); 

    }
  
        System.out.println("iOS driver initiated");
    }

    public void returnAndroidDriver(String browser, String platform, String version, String device) throws MalformedURLException, Exception {
        /** This method returns the Android driver */

            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability("platformName", platform);
            capabilities.setCapability("platformVersion", version);

        if (SAUCELABS_SIMULATOR) {

            System.out.println("Android on remote simulator");

            capabilities.setCapability("name", sTestCase);
            capabilities.setCapability("browserName", browser);
            capabilities.setCapability("deviceName", device);
            capabilities.setCapability("appiumVersion", "1.6.0");
            capabilities.setCapability("deviceOrientation", "portrait");
            capabilities.setCapability("app", "sauce-storage:flapp.zip");
            capabilities.setCapability("appPackage", "za.co.travelstart.flapp");
            capabilities.setCapability("appActivity", "za.co.travelstart.flapp.activities.onboard.InitialActivity");
            capabilities.setCapability("recordScreenshots", recordScreenshots);
            capabilities.setCapability("recordVideo", recordVideo);

            setWebDriver(new AndroidDriver(new URL("http://" + username + ":" + accessKey + "@ondemand.saucelabs.com:80/wd/hub"),
                    capabilities));

        } else if (LOCAL_SIMULATOR) {

            System.out.println("Android on local simulator");

            capabilities.setCapability("appium-version", "1.5.3");
            capabilities.setCapability("name", sTestCase);
            File app = new File(PROJECT_PATH + "/mobile/resources/flapp_v5.1.3_20161102.apk");
            capabilities.setCapability("app", app.getAbsolutePath());
            capabilities.setCapability("deviceName", device);

            setWebDriver(new AndroidDriver(appiummanager.getAppiumUrl(), capabilities));
        }

        else if (SAUCELABS_DEVICE) {

            System.out.println("Android on remote real device");

            capabilities.setCapability("name", sTestCase);
            capabilities.setCapability("browserName", browser);
            capabilities.setCapability("deviceName", device);
            capabilities.setCapability("appiumVersion", "1.5.3");
            capabilities.setCapability("deviceOrientation", "portrait");
            capabilities.setCapability("app", "sauce-storage:flapp.zip");
            capabilities.setCapability("appPackage", "za.co.travelstart.flapp");
            capabilities.setCapability("appActivity", "za.co.travelstart.flapp.activities.onboard.InitialActivity");
            capabilities.setCapability("recordScreenshots", recordScreenshots);
            capabilities.setCapability("recordVideo", recordVideo);

            setWebDriver(new AndroidDriver(new URL("http://" + username + ":" + accessKey + "@ondemand.saucelabs.com:80/wd/hub"),
                    capabilities));
            
        }

        else {
            System.out.println("No driver available"); 

    }

    System.out.println("android driver initiated");

}

    public void returnAndroidMobiSiteDriver(String browser, String platform, String version, String device) throws MalformedURLException, IOException {
        /** This method returns the Android mobi driver */

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities = DesiredCapabilities.android();
        capabilities.setCapability("platformName", platform);
        capabilities.setCapability("platformVersion", version);

        if (SAUCELABS_SIMULATOR) {

            System.out.println("Android on remote simulator");
 
            capabilities.setCapability("appiumVersion", "1.5.3");
            capabilities.setCapability("browserName", browser);
            capabilities.setCapability("deviceName", device);
            capabilities.setCapability("timeZone", "Pacific");
            capabilities.setCapability("name", sTestCase);
            capabilities.setCapability("recordScreenshots", recordScreenshots);
            capabilities.setCapability("recordVideo", recordVideo);

            setWebDriver(new AndroidDriver(new URL("http://" + username + ":" + accessKey + "@ondemand.saucelabs.com:80/wd/hub"), capabilities));

        } else if (LOCAL_SIMULATOR) {

            System.out.println("Android on local simulator");

            capabilities.setCapability("appium-version", "1.5.3");
            capabilities.setCapability("browserName", browser);
            capabilities.setCapability("deviceName", device);
            //Use Chrome if emulator has Chrome 
            //capabilities.setCapability("browserName", "Chrome");
            //capabilities.setCapability("appPackage", "com.android.chrome");
            //capabilities.setCapability("appActivity", "com.android.chrome.Main");

            setWebDriver(new AndroidDriver(appiummanager.getAppiumUrl(), capabilities));

        }

         else if (SAUCELABS_DEVICE) {

            System.out.println("Android on remote real device");

            capabilities.setCapability("browserName", browser);
            capabilities.setCapability("deviceName", device);
            capabilities.setCapability("appiumVersion", "1.6.0");
            capabilities.setCapability("deviceOrientation", "portrait");
            capabilities.setCapability("app", "sauce-storage:flapp.zip.zip");
            capabilities.setCapability("appPackage", "za.co.travelstart.flapp");
            capabilities.setCapability("appActivity", "za.co.travelstart.flapp.activities.onboard.InitialActivity");
            capabilities.setCapability("recordScreenshots", recordScreenshots);
            capabilities.setCapability("recordVideo", recordVideo);

            setWebDriver(new AndroidDriver(new URL("http://" + username + ":" + accessKey + "@ondemand.saucelabs.com:80/wd/hub"),
                    capabilities));

        } else {
            System.out.println("No driver available"); 

    }

        getDriver().get(MOBI_SERVER);
        System.out.println("Mobile Site = " + MOBI_SERVER);
        System.out.println("Mobi browser on Android started");

}

  public void returniOSMobiSiteDriver(String browser, String platform, String version, String device) throws MalformedURLException, IOException {
    /** This method returns the iOS mobi driver */

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", platform);
        capabilities.setCapability("platformVersion", version);

        if (SAUCELABS_SIMULATOR) {

            System.out.println("iOS on remote simulator");
 
            capabilities.setCapability("appiumVersion", "1.6.0");
            capabilities.setCapability("browserName", browser);
            capabilities.setCapability("name", sTestCase);
            capabilities.setCapability("deviceName", device);
            capabilities.setCapability("recordScreenshots", recordScreenshots);
            capabilities.setCapability("recordVideo", recordVideo);

            setWebDriver(new IOSDriver(new URL("http://" + username + ":" + accessKey + "@ondemand.saucelabs.com:80/wd/hub"),
                    capabilities));

        } else if (LOCAL_SIMULATOR) {

            System.out.println("iOS on local simulator");

            capabilities.setCapability("browserName", browser);
            capabilities.setCapability("appium-version", "1.5.3");
            capabilities.setCapability("fullReset", true);

            setWebDriver(new IOSDriver(appiummanager.getAppiumUrl(), capabilities));

        }

        else if (SAUCELABS_DEVICE) {

            System.out.println("iOS on remote real device");

            capabilities.setCapability(CapabilityType.BROWSER_NAME, "");
            capabilities.setCapability("deviceName", device);
            capabilities.setCapability("browserName", browser);
            capabilities.setCapability("recordScreenshots", recordScreenshots);
            capabilities.setCapability("recordVideo", recordVideo);
            capabilities.setCapability("appiumVersion", "1.6.0");

            setWebDriver(new IOSDriver(new URL("http://" + username + ":" + accessKey + "@ondemand.saucelabs.com:80/wd/hub"),
                    capabilities));

        } else {
            System.out.println("No driver available"); 

    }

        getDriver().get(MOBI_SERVER);
        System.out.println("Mobile Site = " + MOBI_SERVER);
        System.out.println("Mobi browser on iOS started");

}

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) throws Exception {
        /** This method closes the driver */

        if (SAUCELABS_SIMULATOR || SAUCELABS_DEVICE)  {
            System.out.println("Update SauceLabs result");
            String jobID = ((RemoteWebDriver) getDriver()).getSessionId().toString();
            System.out.println("Session ID: " + jobID);
            SauceREST client = new SauceREST(username, accessKey);
            Map<String, Object> sauceJob = new HashMap<String, Object>();
            if (result.isSuccess()) {
                client.jobPassed(jobID);
            } else {
                client.jobFailed(jobID);
            }
            client.updateJobInfo(jobID, sauceJob);
            System.out.println("Inside quit");
            getDriver().quit();

        } else if (LOCAL_DEVICE) {
            appiumparalleltest.endLogTestResults(result);
            getDriver().quit();

        } else {
            System.out.println("Inside quit");
            getDriver().quit();
        }
    }

    @AfterClass()
    public void afterClass() throws InterruptedException, IOException {
        if (LOCAL_DEVICE)  {
        appiumparalleltest.killAppiumServer();
      
    } else {
        System.out.println("No server to kill");
  }

 }

}
